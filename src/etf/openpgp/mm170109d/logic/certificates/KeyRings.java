package etf.openpgp.mm170109d.logic.certificates;

import org.bouncycastle.bcpg.ArmoredInputStream;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.bcpg.sig.KeyFlags;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.operator.PBESecretKeyDecryptor;
import org.bouncycastle.openpgp.operator.PBESecretKeyEncryptor;
import org.bouncycastle.openpgp.operator.PGPDigestCalculator;
import org.bouncycastle.openpgp.operator.bc.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class KeyRings
{
	private static final String PUBLIC_KEYS_FILE_LOCATION = "Keys/publicKeyringCollection.asc";
	private static final String SECRET_KEYS_FILE_LOCATION = "Keys/secretKeyringCollection.asc";

	private static PGPPublicKeyRingCollection publicKeyStorage;
	private static PGPSecretKeyRingCollection secretKeyStorage;

	private static void generateKeyFiles()
	{
		char[] password = { 'p', 'a', 's', 's' };
		PGPKeyRingGenerator gen;

		try
		{
			gen = generateNewKeyPair( "Mirko <mirko@gmail.com>", password, 1024 );
			PGPSecretKeyRing secretKeyRing = gen.generateSecretKeyRing();
			secretKeyStorage = PGPSecretKeyRingCollection.addSecretKeyRing(
					secretKeyStorage, secretKeyRing );

			gen = generateNewKeyPair( "Slavko <slavko@gmail.com>", password, 2048 );
			secretKeyRing = gen.generateSecretKeyRing();
			secretKeyStorage = PGPSecretKeyRingCollection.addSecretKeyRing(
					secretKeyStorage, secretKeyRing );

			///////

			ArrayList< PGPPublicKey > list = new ArrayList<>();
			Iterator< PGPPublicKey > it = secretKeyRing.getPublicKeys();
			list.add( it.next() );
			list.add( it.next() );
			PGPPublicKeyRing ring = new PGPPublicKeyRing( list );
			publicKeyStorage = PGPPublicKeyRingCollection.addPublicKeyRing(
					publicKeyStorage, ring );

			///////

			gen = generateNewKeyPair( "Rajko <rajko@gmail.com>", password, 4096 );
			secretKeyRing = gen.generateSecretKeyRing();
			secretKeyStorage = PGPSecretKeyRingCollection.addSecretKeyRing(
					secretKeyStorage, secretKeyRing );

			gen = generateNewKeyPair( "Sanja <sanja@gmail.com>", password, 2048 );
			PGPPublicKeyRing publicKey = gen.generatePublicKeyRing();
			publicKeyStorage = PGPPublicKeyRingCollection.addPublicKeyRing(
					publicKeyStorage, publicKey );

			gen = generateNewKeyPair( "Anja <anja@gmail.com>", password, 2048 );
			publicKey = gen.generatePublicKeyRing();
			publicKeyStorage = PGPPublicKeyRingCollection.addPublicKeyRing(
					publicKeyStorage, publicKey );

		} catch( Exception e )
		{
			e.printStackTrace();
		}
	}

	public static void saveKeyFiles()
	{
		File publicKeyFile, secretKeyFile;
		FileOutputStream publicFos = null, secretFos = null;
		ArmoredOutputStream armorSecretOut = null, armorPublicOut = null;

		try
		{
			publicKeyFile = new File( PUBLIC_KEYS_FILE_LOCATION );
			secretKeyFile = new File( SECRET_KEYS_FILE_LOCATION );

			if( !publicKeyFile.exists() )
			{
				publicKeyFile.createNewFile();
			}
			if( !secretKeyFile.exists() )
			{
				secretKeyFile.createNewFile();
			}

			publicFos = new FileOutputStream( publicKeyFile );
			secretFos = new FileOutputStream( secretKeyFile );
			armorPublicOut = new ArmoredOutputStream( publicFos );
			armorSecretOut = new ArmoredOutputStream( secretFos );

			armorPublicOut.write( publicKeyStorage.getEncoded() );
			armorSecretOut.write( secretKeyStorage.getEncoded() );
			armorPublicOut.flush();
			armorSecretOut.flush();
		} catch( Exception e )
		{
			e.printStackTrace();
		} finally
		{
			try
			{
				armorPublicOut.close();
				armorSecretOut.close();
				publicFos.close();
				secretFos.close();
			} catch( IOException ioe )
			{
				System.out.println( "Error in closing the Stream" );
			}
		}
	}

	public static PGPPublicKeyRingCollection getPublicKeyStorage()
	{
		return publicKeyStorage;
	}

	public static PGPSecretKeyRingCollection getSecretKeyStorage()
	{
		return secretKeyStorage;
	}

	public static void init() throws PGPException, IOException
	{

		try
		{
			publicKeyStorage = new PGPPublicKeyRingCollection(
					PGPUtil.getDecoderStream( new FileInputStream( PUBLIC_KEYS_FILE_LOCATION ) ),
					new BcKeyFingerprintCalculator()
			);
			secretKeyStorage = new PGPSecretKeyRingCollection(
					PGPUtil.getDecoderStream( new FileInputStream( SECRET_KEYS_FILE_LOCATION ) ),
					new BcKeyFingerprintCalculator()
			);
		} catch( Exception e )
		{
			publicKeyStorage = new PGPPublicKeyRingCollection( new ArrayList< PGPPublicKeyRing >() );
			secretKeyStorage = new PGPSecretKeyRingCollection( new ArrayList< PGPSecretKeyRing >() );
		}

		//generateKeyFiles();
		//saveKeyFiles();
	}

	public static PGPSecretKey createNewSecretKey( String id, char[] pass, int keySize )
	{
		try
		{
			PGPKeyRingGenerator gen = generateNewKeyPair( id, pass, keySize );
			PGPSecretKeyRing secretKeyRing = gen.generateSecretKeyRing();
			secretKeyStorage = PGPSecretKeyRingCollection.addSecretKeyRing(
					secretKeyStorage, secretKeyRing );
			return secretKeyRing.getSecretKey();
		} catch( Exception e )
		{
			e.printStackTrace();
		}
		return null;
	}

	public static void removeSecretKey( long id )
	{
		try
		{
			secretKeyStorage = PGPSecretKeyRingCollection.removeSecretKeyRing(
					secretKeyStorage, secretKeyStorage.getSecretKeyRing( id )
			);
		} catch( PGPException e )
		{
			e.printStackTrace();
		}
	}

	public static void removePublicKey( long id )
	{
		try
		{
			publicKeyStorage = PGPPublicKeyRingCollection.removePublicKeyRing(
					publicKeyStorage, publicKeyStorage.getPublicKeyRing( id )
			);
		} catch( PGPException e )
		{
			e.printStackTrace();
		}
	}

	private static PGPKeyRingGenerator generateNewKeyPair( String id, char[] pass, int keySize )
			throws Exception
	{
		RSAKeyPairGenerator rsaGen = new RSAKeyPairGenerator();
		rsaGen.init( new RSAKeyGenerationParameters( BigInteger.valueOf( 0x10001 ), new SecureRandom(),
				keySize, 12 ) );

		PGPKeyPair sinKey = new BcPGPKeyPair( PGPPublicKey.RSA_SIGN, rsaGen.generateKeyPair(),
				new Date() );
		PGPKeyPair encKey = new BcPGPKeyPair( PGPPublicKey.RSA_ENCRYPT, rsaGen.generateKeyPair(),
				new Date() );

		//signature
		long validPeriod = 60 * 60 * 24 * 2;
		validPeriod *= 365; //2 years

		PGPSignatureSubpacketGenerator signGenSin = new PGPSignatureSubpacketGenerator();
		signGenSin.setKeyExpirationTime( true, validPeriod );

		signGenSin.setKeyFlags( false, KeyFlags.SIGN_DATA | KeyFlags.CERTIFY_OTHER );
		signGenSin.setPreferredSymmetricAlgorithms
				( false, new int[]{
						SymmetricKeyAlgorithmTags.TRIPLE_DES,
						SymmetricKeyAlgorithmTags.AES_128
				} );
		signGenSin.setPreferredHashAlgorithms
				( false, new int[]{
						HashAlgorithmTags.SHA1
				} );

		PGPSignatureSubpacketGenerator signGenEnc = new PGPSignatureSubpacketGenerator();
		signGenEnc.setKeyExpirationTime( true, validPeriod );
		signGenEnc.setKeyFlags( false, KeyFlags.ENCRYPT_COMMS | KeyFlags.ENCRYPT_STORAGE );

		PGPDigestCalculator sha1Calc = new BcPGPDigestCalculatorProvider()
				.get( HashAlgorithmTags.SHA1 );
		PGPDigestCalculator sha256Calc = new BcPGPDigestCalculatorProvider()
				.get( HashAlgorithmTags.SHA256 );

		PBESecretKeyEncryptor encryptor = (
				new BcPBESecretKeyEncryptorBuilder(
						PGPEncryptedData.AES_256, sha256Calc, 0xc0 ) )
				.build( pass );

		PGPKeyRingGenerator keyRingGen = new PGPKeyRingGenerator( PGPSignature.POSITIVE_CERTIFICATION,
				sinKey, id, sha1Calc, signGenSin.generate(), null,
				new BcPGPContentSignerBuilder( sinKey.getPublicKey().getAlgorithm(),
						HashAlgorithmTags.SHA1 ), encryptor );

		keyRingGen.addSubKey( encKey, signGenEnc.generate(), null );
		return keyRingGen;
	}

	public static List< PGPPublicKey > getPublicKeys()
	{
		List< PGPPublicKey > list = new ArrayList<>();
		publicKeyStorage.forEach( keyRing -> list.add( keyRing.getPublicKey() ) );
		return list;
	}

	public static List< PGPPublicKey > getPublicEncKeys()
	{
		List< PGPPublicKey > list = new ArrayList<>();
		publicKeyStorage.forEach( keyRing ->
		{
			Iterator< PGPPublicKey > keyIter = keyRing.getPublicKeys();
			keyIter.next();
			list.add( keyIter.next() );
		} );
		return list;
	}

	public static List< PGPSecretKey > getSecretKeys()
	{
		List< PGPSecretKey > list = new ArrayList<>();
		secretKeyStorage.forEach( keyRing -> list.add( keyRing.getSecretKey() ) );
		return list;
	}

	public static void exportKey( PGPPublicKeyRing publicKeyRing, PGPSecretKeyRing secretKeyRing, String path )
	{
		FileOutputStream fos = null;
		File file;
		ArmoredOutputStream armorOut = null;
		String name;
		byte[] bytesArray;

		try
		{
			if( publicKeyRing != null )
			{
				name = publicKeyRing.getPublicKey().getUserIDs().next() + ".asc";
				bytesArray = publicKeyRing.getEncoded();
			} else if( secretKeyRing != null )
			{
				name = secretKeyRing.getPublicKey().getUserIDs().next() + ".asc";
				bytesArray = secretKeyRing.getEncoded();
			} else
			{
				return;
			}

			file = new File( path + "\\" +
					name.replace( '<', '_' ).replace( '>', '_' ) );
			if( !file.exists() )
			{
				file.createNewFile();
			}

			fos = new FileOutputStream( file );
			armorOut = new ArmoredOutputStream( fos );

			armorOut.write( bytesArray );
			armorOut.flush();

		} catch( IOException ioe )
		{
			ioe.printStackTrace();
		} finally
		{
			try
			{
				if( fos != null )
				{
					armorOut.close();
					fos.close();
				}
			} catch( IOException ioe )
			{
				System.out.println( "Error in closing the Stream" );
			}
		}
	}

	public static PGPPublicKey importKey( String fileLocation )
	{
		if( fileLocation == null )
			return null;

		FileInputStream fis = null;
		File file;
		ArmoredInputStream armorIn = null;

		try
		{
			file = new File( fileLocation );
			if( !file.exists() )
			{
				return null;
			}

			fis = new FileInputStream( file );
			armorIn = new ArmoredInputStream( fis );

			byte[] bytesArray = armorIn.readAllBytes();

			PGPPublicKeyRing newKeyRing = new PGPPublicKeyRing( bytesArray, new BcKeyFingerprintCalculator() );
			publicKeyStorage = PGPPublicKeyRingCollection.addPublicKeyRing( publicKeyStorage, newKeyRing );
			return newKeyRing.getPublicKey();
		} catch( IOException ioe )
		{
			ioe.printStackTrace();
		} finally
		{
			try
			{
				if( fis != null )
				{
					armorIn.close();
					fis.close();
				}
			} catch( IOException ioe )
			{
				System.out.println( "Error in closing the Stream" );
			}
		}
		return null;
	}

	public static PGPPublicKey getPublicKey( long id ) throws PGPException
	{
		return publicKeyStorage.getPublicKey( id );
	}

	public static PGPSecretKey getSecretKey( long id ) throws PGPException
	{
		return secretKeyStorage.getSecretKey( id );
	}

	public static PGPPublicKeyRing getPublicKeyRing( long id ) throws PGPException
	{
		return publicKeyStorage.getPublicKeyRing( id );
	}

	public static PGPSecretKeyRing getSecretKeyRing( long id ) throws PGPException
	{
		return secretKeyStorage.getSecretKeyRing( id );
	}

	public static boolean checkPasswordOk( long id, char[] password )
	{
		PGPSecretKey secretKey = null;
		try
		{
			PBESecretKeyDecryptor decryptor = new BcPBESecretKeyDecryptorBuilder( new BcPGPDigestCalculatorProvider() )
					.build( password );
			secretKey = secretKeyStorage.getSecretKey( id );
			secretKey.extractPrivateKey( decryptor );
			return true;
		} catch( PGPException e )
		{
			return false;
		}
	}

	public static PGPPrivateKey getPrivateFromSecretKey( PGPSecretKey secretKey, char[] password )
			throws PGPException
	{
		if( secretKey == null )
			return null;

		PBESecretKeyDecryptor decryptor = new BcPBESecretKeyDecryptorBuilder( new BcPGPDigestCalculatorProvider() )
				.build( password );
		return secretKey.extractPrivateKey( decryptor );
	}
}
