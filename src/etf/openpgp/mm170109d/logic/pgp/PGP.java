package etf.openpgp.mm170109d.logic.pgp;

import etf.openpgp.mm170109d.logic.certificates.KeyRings;
import javafx.geometry.Pos;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.CompressionAlgorithmTags;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.bc.*;

import java.io.*;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class PGP
{

	//COMPRESSION - CompressionAlgorithmTags.ZIP
	//Symetric encryption - PGPEncryptedData.AES_128 || PGPEncryptedData.TRIPLE_DES

	static final int BUFFER_SIZE = 1 << 16;

	public static class ProcessedFileData
	{
		public enum ERROR_CODES
		{
			OK,
			WRONG_PASSWORD,
			DEGENERATE_SIGNATURE_NO_ORIGINAL_FILE,
			NO_KEY_AVAILABLE,
			MESSAGE_INTEGRITY_CHECK_FALIED,
			SIGNATURE_VERIFY_FAILED,
			SIGNER_UNKNOWN
		}

		;

		public boolean encrypted;
		public boolean signed;
		public boolean degenereateSig;
		public int encAlgorithm;
		public int signAlgorithm;
		public int keyStrength;
		public ERROR_CODES errorCode;
		public String user;
		public byte[] data;
	}

	public static ProcessedFileData analyzePGPFile( String file )
			throws IOException, NoSuchProviderException
	{
		InputStream in = new BufferedInputStream( new FileInputStream( file ) );
		in = PGPUtil.getDecoderStream( in );

		ProcessedFileData processedFileData = new ProcessedFileData();
		processedFileData.errorCode = ProcessedFileData.ERROR_CODES.OK;

		try
		{
			JcaPGPObjectFactory pgpObjectFactory = new JcaPGPObjectFactory( in );
			PGPEncryptedDataList encryptedDataList = null;
			PGPPublicKeyEncryptedData publicKeyEncryptedData = null;
			PGPOnePassSignatureList signatureList = null;
			PGPOnePassSignature signature = null;
			PGPSignatureList pgpSignatures = null;
			PGPPublicKey key = null;
			int ch;
			boolean signed = false;

			Object object = pgpObjectFactory.nextObject();

			if( object instanceof PGPSignatureList )
			{
				processedFileData.signed = true;
				processedFileData.degenereateSig = true;
				File originalFile = new File( file.substring( 0, file.lastIndexOf( '.' ) ) );

				if( !originalFile.exists() )
				{
					processedFileData.errorCode = ProcessedFileData.ERROR_CODES.DEGENERATE_SIGNATURE_NO_ORIGINAL_FILE;
					return processedFileData;
				}

				InputStream fileIn = new BufferedInputStream( new FileInputStream( originalFile ) );

				PGPSignatureList sigList = ( PGPSignatureList ) object;
				PGPSignature sig = sigList.get( 0 );

				key = KeyRings.getPublicKey( sig.getKeyID() );
				sig.init( new BcPGPContentVerifierBuilderProvider(), key );
				processedFileData.user = key.getUserIDs().next();

				ByteArrayOutputStream out = new ByteArrayOutputStream();

				while( ( ch = fileIn.read() ) >= 0 )
				{
					sig.update( ( byte ) ch );
					out.write( ch );
				}

				if( sig.verify() == false )
				{
					processedFileData.errorCode = ProcessedFileData.ERROR_CODES.SIGNATURE_VERIFY_FAILED;
				}

				processedFileData.data = out.toByteArray();
				processedFileData.signAlgorithm = sig.getHashAlgorithm();
				return processedFileData;
			}

			if( object instanceof PGPEncryptedDataList )
			{
				processedFileData.encrypted = true;
				encryptedDataList = ( PGPEncryptedDataList ) object;

				Iterator it = encryptedDataList.getEncryptedDataObjects();
				PGPPrivateKey privateKey = null;
				PGPSecretKey secretKey = null;
				publicKeyEncryptedData = null;

				while( privateKey == null && it.hasNext() )
				{
					publicKeyEncryptedData = ( PGPPublicKeyEncryptedData ) it.next();

					PGPSecretKeyRing secretKeyRing = KeyRings.getSecretKeyRing( publicKeyEncryptedData.getKeyID() );
					secretKey = KeyRings.getSecretKey( publicKeyEncryptedData.getKeyID() );

					if( secretKey != null )
					{
						char[] pass;
						Dialog< String > dialog = new Dialog<>();
						Iterator< String > userId = secretKeyRing.getPublicKey().getUserIDs();
						String user = "unknown";
						if( userId.hasNext() )
						{
							user = userId.next();
						}
						dialog.setTitle( "Enter password" );
						( ( Stage ) dialog.getDialogPane().getScene().getWindow() ).getIcons()
								.add( new Image( "etf/openpgp/mm170109d/graphics/scene_keys/images/key.png" ) );
						dialog.getDialogPane().getButtonTypes().addAll( ButtonType.OK, ButtonType.CANCEL );

						PasswordField passwordField = new PasswordField();
						HBox content = new HBox();
						content.setAlignment( Pos.CENTER_LEFT );
						content.setSpacing( 10 );
						content.getChildren().addAll( new Label( "Password:" ), passwordField );
						VBox vbox = new VBox();
						vbox.setSpacing( 5.0 );
						vbox.setAlignment( Pos.CENTER );
						vbox.getChildren().addAll(
								new Label( "Enter password for " + user + "'s key!" ),
								content
						);

						dialog.getDialogPane().setContent( vbox );
						dialog.setResultConverter( dialogButton ->
						{
							if( dialogButton == ButtonType.OK )
							{
								return passwordField.getText();
							}
							return null;
						} );

						Optional< String > passResult = dialog.showAndWait();
						if( passResult.isPresent() )
						{
							pass = passResult.get().toCharArray();
						} else
						{
							processedFileData.errorCode = ProcessedFileData.ERROR_CODES.WRONG_PASSWORD;
							return processedFileData;
						}

						if( KeyRings.checkPasswordOk( secretKey.getKeyID(), pass ) )
						{
							privateKey = KeyRings.getPrivateFromSecretKey( secretKey, pass );
						} else
						{
							processedFileData.errorCode = ProcessedFileData.ERROR_CODES.WRONG_PASSWORD;
							return processedFileData;
						}
					}
				}

				if( privateKey == null )
				{
					processedFileData.errorCode = ProcessedFileData.ERROR_CODES.NO_KEY_AVAILABLE;
					return processedFileData;
				}

				InputStream clear = publicKeyEncryptedData.getDataStream( new BcPublicKeyDataDecryptorFactory( privateKey ) );
				pgpObjectFactory = new JcaPGPObjectFactory( clear );

				object = pgpObjectFactory.nextObject();

				processedFileData.encAlgorithm = publicKeyEncryptedData.getSymmetricAlgorithm(
						new BcPublicKeyDataDecryptorFactory( privateKey )
				);
				processedFileData.keyStrength = secretKey.getPublicKey().getBitStrength();
			}

			if( object instanceof PGPCompressedData )
			{
				PGPCompressedData cData = ( PGPCompressedData ) object;
				pgpObjectFactory = new JcaPGPObjectFactory( cData.getDataStream() );

				object = pgpObjectFactory.nextObject();
			}

			if( object instanceof PGPOnePassSignatureList )
			{
				processedFileData.signed = true;
				processedFileData.degenereateSig = false;
				signatureList = ( PGPOnePassSignatureList ) object;
				signature = signatureList.get( 0 );

				processedFileData.signAlgorithm = signature.getHashAlgorithm();

				object = pgpObjectFactory.nextObject();
				//pgpSignatures = ( PGPSignatureList ) pgpObjectFactory.nextObject();
				signed = true;
			}

			if( object instanceof PGPLiteralData )
			{
				PGPLiteralData ld = ( PGPLiteralData ) object;

				String outFileName = ld.getFileName();
				if( outFileName.length() == 0 )
				{
					outFileName = file.substring( 0, file.lastIndexOf( '.' ) );
				}

				InputStream dIn = ld.getInputStream();

				if( signed )
				{
					//PGPPublicKeyRingCollection pgpRing = KeyRings.getPublicKeyStorage();
					//key = pgpRing.getPublicKey( signature.getKeyID() );
					key = KeyRings.getPublicKey( signature.getKeyID() );
					if( key == null )
					{
						PGPSecretKeyRing secretKeyRing =
								KeyRings.getSecretKeyStorage().getSecretKeyRing( signature.getKeyID() );
						if( secretKeyRing != null )
							key = secretKeyRing.getPublicKey();
					}
					if( key != null )
					{
						signature.init( new BcPGPContentVerifierBuilderProvider(), key );
						processedFileData.user = key.getUserIDs().next();
					} else
					{
						processedFileData.errorCode = ProcessedFileData.ERROR_CODES.SIGNER_UNKNOWN;
						signed = false;
					}
				}

				//FileOutputStream out = new FileOutputStream( outFileName );
				ByteArrayOutputStream out = new ByteArrayOutputStream();

				while( ( ch = dIn.read() ) >= 0 )
				{
					if( signed )
					{
						signature.update( ( byte ) ch );
					}
					out.write( ch );
				}

				out.close();

				processedFileData.data = out.toByteArray();

				pgpSignatures = ( PGPSignatureList ) pgpObjectFactory.nextObject();

				if( publicKeyEncryptedData != null && publicKeyEncryptedData.isIntegrityProtected() )
				{
					if( !publicKeyEncryptedData.verify() )
					{
						processedFileData.errorCode = ProcessedFileData.ERROR_CODES.MESSAGE_INTEGRITY_CHECK_FALIED;
						return processedFileData;
					}
				}

				if( signed )
				{
					if( !signature.verify( pgpSignatures.get( 0 ) ) )
					{
						processedFileData.errorCode = ProcessedFileData.ERROR_CODES.SIGNATURE_VERIFY_FAILED;
						return processedFileData;
					}
				}

			}
		} catch( Exception e )
		{
			e.printStackTrace();
		} finally
		{
			in.close();
		}

		return processedFileData;
	}

	public static void executePGP( String inputFileName, String outputFileName, int alg,
								   List< PGPPublicKey > keys, PGPSecretKey secretKey, char[] pass,
								   boolean enc, boolean sign, boolean comp, boolean radix64 )
			throws IOException, PGPException
	{
		OutputStream out = new BufferedOutputStream(
				new FileOutputStream( outputFileName )
		);
		File file = new File( inputFileName );
		FileInputStream fIn = new FileInputStream( file );

		if( radix64 )
		{
			out = new ArmoredOutputStream( out );
		}
		OutputStream baseOut = out;

		PGPEncryptedDataGenerator encryptedDataGenerator = null;

		if( enc )
		{
			encryptedDataGenerator = new PGPEncryptedDataGenerator(
					new BcPGPDataEncryptorBuilder( alg )
							.setWithIntegrityPacket( true )
							.setSecureRandom( new SecureRandom() )
			);

			for( PGPPublicKey key : keys )
			{
				encryptedDataGenerator.addMethod(
						new BcPublicKeyKeyEncryptionMethodGenerator( key )
				);
			}

			out = encryptedDataGenerator.open( out, new byte[ BUFFER_SIZE ] );
		}

		PGPCompressedDataGenerator compressedDataGenerator = null;
		if( comp )
		{
			compressedDataGenerator = new PGPCompressedDataGenerator(
					CompressionAlgorithmTags.ZIP
			);

			out = compressedDataGenerator.open( out );
		}

		PGPSignatureGenerator signatureGenerator = null;
		if( sign )
		{
			PGPPrivateKey privateKey = KeyRings.getPrivateFromSecretKey( secretKey, pass );

			signatureGenerator = new PGPSignatureGenerator(
					new BcPGPContentSignerBuilder(
							secretKey.getPublicKey()
									.getAlgorithm(), PGPUtil.SHA1
					)
			);

			signatureGenerator.init( PGPSignature.BINARY_DOCUMENT, privateKey );

			Iterator it = secretKey.getPublicKey().getUserIDs();
			if( it.hasNext() )
			{
				PGPSignatureSubpacketGenerator spGen =
						new PGPSignatureSubpacketGenerator();

				spGen.setSignerUserID( false, ( String ) it.next() );
				signatureGenerator.setHashedSubpackets( spGen.generate() );
			}

			signatureGenerator.generateOnePassVersion( false ).encode( out );
		}

		int ch;

		//if( enc || comp )
		//{
		PGPLiteralDataGenerator lGen = new PGPLiteralDataGenerator();
		OutputStream lOut = lGen.open( out, PGPLiteralData.BINARY, file );

		while( ( ch = fIn.read() ) >= 0 )
		{
			lOut.write( ch );
			if( sign )
				signatureGenerator.update( ( byte ) ch );
		}

		lGen.close();/*
        }
        else
        {
            while ((ch = fIn.read()) >= 0) {
                out.write( ch );
                if ( sign )
                    signatureGenerator.update( (byte) ch );
            }
        }
        */

		if( sign )
		{
			signatureGenerator.generate().encode( out );
		}

		if( comp ) compressedDataGenerator.close();
		if( enc ) encryptedDataGenerator.close();
		baseOut.close();
	}

}
