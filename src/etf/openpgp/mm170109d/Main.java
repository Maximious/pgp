package etf.openpgp.mm170109d;

import etf.openpgp.mm170109d.graphics.scene_keys.models.PrivateKeyModel;
import etf.openpgp.mm170109d.graphics.scene_keys.models.PublicKeyModel;
import etf.openpgp.mm170109d.logic.certificates.KeyRings;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.security.Key;

public class Main extends Application
{

	@Override
	public void start( Stage primaryStage ) throws Exception
	{
		Parent root = FXMLLoader.load( getClass().getResource( "graphics/scene_main/scene.fxml" ) );
		primaryStage.setTitle( "PGP protocol" );
		primaryStage.setScene( new Scene( root ) );
		primaryStage.getIcons().add( new Image( "etf/openpgp/mm170109d/graphics/scene_main/images/lock.png" ) );
		primaryStage.show();

		KeyRings.init();
	}

	@Override
	public void stop() throws Exception
	{
		super.stop();
		KeyRings.saveKeyFiles();
	}

	public static void main( String[] args )
	{
		launch( args );
	}
}
