package etf.openpgp.mm170109d.graphics.scene_rec;

import etf.openpgp.mm170109d.logic.pgp.PGP;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.openpgp.PGPEncryptedData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class Controller
{
	private static final HashMap< Integer, String > SYMETRIC_ALGORITHMS;
	private static final HashMap< Integer, String > HASH_ALGORITHMS;

	static
	{
		SYMETRIC_ALGORITHMS = new HashMap< Integer, String >();
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.AES_128, "AES_128" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.AES_192, "AES_192" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.AES_256, "AES_256" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.BLOWFISH, "BLOWFISH" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.CAMELLIA_128, "CAMELLIA_128" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.CAMELLIA_192, "CAMELLIA_192" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.CAMELLIA_256, "CAMELLIA_256" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.CAST5, "CAST5" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.DES, "DES" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.IDEA, "IDEA" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.SAFER, "SAFER" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.TRIPLE_DES, "TRIPLE_DES" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.TWOFISH, "TWOFISH" );
		SYMETRIC_ALGORITHMS.put( PGPEncryptedData.NULL, "NULL" );

		HASH_ALGORITHMS = new HashMap< Integer, String >();
		HASH_ALGORITHMS.put( HashAlgorithmTags.DOUBLE_SHA, "DOUBLE_SHA" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.HAVAL_5_160, "HAVAL_5_160" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.MD2, "MD2" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.MD5, "MD5" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.SHA1, "SHA1" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.SHA224, "SHA224" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.SHA256, "SHA256" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.SHA384, "SHA384" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.SHA512, "SHA512" );
		HASH_ALGORITHMS.put( HashAlgorithmTags.TIGER_192, "TIGER_192" );
	}

	private PGP.ProcessedFileData processedFileData = null;

	@FXML
	private TextField inputFileLocation;
	@FXML
	private TextArea messageTextArea;
	@FXML
	private Label encLabel;
	@FXML
	private Label signLabel;

	private static final String EMPTY_LABEL = "-";

	public void mainScene( MouseEvent mouseEvent ) throws IOException
	{
		Parent root = FXMLLoader.load( getClass().getResource( "../scene_main/scene.fxml" ) );
		Stage stage = ( Stage ) ( ( Node ) mouseEvent.getSource() ).getScene().getWindow();
		stage.getScene().setRoot( root );
		stage.show();
	}

	public void setInputFile( MouseEvent mouseEvent )
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle( "Select input file" );
		Stage stage = new Stage();
		String path = fileChooser.showOpenDialog( stage ).getPath();

		if( path != null )
		{
			inputFileLocation.setText( path );
		}
	}

	public void startPGP( MouseEvent mouseEvent ) throws Exception
	{
		processedFileData = PGP.analyzePGPFile( inputFileLocation.getText() );

		encLabel.setText( EMPTY_LABEL );
		signLabel.setText( EMPTY_LABEL );
		messageTextArea.setText( "" );

		if( processedFileData.errorCode != PGP.ProcessedFileData.ERROR_CODES.OK &&
				processedFileData.errorCode != PGP.ProcessedFileData.ERROR_CODES.SIGNER_UNKNOWN )
		{
			messageTextArea.setText( "Error occurred - " + processedFileData.errorCode.name() );
		} else
		{
			if( processedFileData.data != null )
				messageTextArea.setText( new String( processedFileData.data ) );

			if( processedFileData.encrypted )
			{
				encLabel.setText( "Encrypted using " + SYMETRIC_ALGORITHMS.get( processedFileData.encAlgorithm )
						+ " " + processedFileData.keyStrength + "b key. Integrity checked." );
			} else
			{
				encLabel.setText( EMPTY_LABEL );
			}

			if( processedFileData.signed )
			{
				String user = processedFileData.user != null ? processedFileData.user : "unknown";
				signLabel.setText( "Signed by " + user + " using " +
						HASH_ALGORITHMS.get( processedFileData.signAlgorithm ) + " algorithm." );
				if( processedFileData.degenereateSig )
					signLabel.setText( signLabel.getText() + "Signature only." );
			} else
			{
				signLabel.setText( EMPTY_LABEL );
			}
		}
	}

	public void save( MouseEvent mouseEvent ) throws IOException
	{
		if( processedFileData.data != null && processedFileData.degenereateSig == false )
		{
			DirectoryChooser directoryChooser = new DirectoryChooser();
			directoryChooser.setTitle( "Select output location" );
			Stage stage = new Stage();
			String path = directoryChooser.showDialog( stage ).getPath();

			if( path != null )
			{
				String inputFile = inputFileLocation.getText();
				inputFile = inputFile.substring( inputFile.lastIndexOf( "\\" ) + 1 );
				String outputFile = path + "\\" + inputFile.substring( 0, inputFile.lastIndexOf( "." ) );
				File file = new File( outputFile );
				if( !file.exists() )
				{
					file.createNewFile();
				}
				FileOutputStream fOut = new FileOutputStream( file );
				fOut.write( processedFileData.data );
				fOut.close();
			}
		}
	}
}
