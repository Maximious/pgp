package etf.openpgp.mm170109d.graphics.scene_keys.scene_dialog_key;

public class SceneDialogKeyResult
{

	private String id;
	private int keySize;

	public SceneDialogKeyResult()
	{
	}

	public SceneDialogKeyResult( String id, int keySize )
	{
		this.id = id;
		this.keySize = keySize;
	}

	public String getId()
	{
		return id;
	}

	public void setId( String id )
	{
		this.id = id;
	}

	public int getKeySize()
	{
		return keySize;
	}

	public void setKeySize( int keySize )
	{
		this.keySize = keySize;
	}
}
