package etf.openpgp.mm170109d.graphics.scene_keys.scene_dialog_key;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;

public class SceneDialogKey extends AnchorPane
{

	private static int keySizeInt[] = { 1024, 2048, 4096 };

	@FXML
	private TextField name;
	@FXML
	private TextField mail;
	@FXML
	private RadioButton smallKey;
	@FXML
	private RadioButton mediumKey;
	@FXML
	private RadioButton largeKey;
	@FXML
	private Button acceptButton;
	@FXML
	private Button cancelButton;

	private SceneDialogKeyResult result = null;

	public SceneDialogKey() throws IOException
	{
		FXMLLoader loader = new FXMLLoader( getClass().getResource( "scene.fxml" ) );
		loader.setRoot( this );
		loader.setController( this );
		loader.load();

		acceptButton.setOnMouseClicked( event ->
		{
			result = new SceneDialogKeyResult();
			int keySize = mediumKey.isSelected() ? 2048 : 1024;
			keySize = largeKey.isSelected() ? 4096 : keySize;
			result.setId( name.getText() + " <" + mail.getText() + ">" );
			result.setKeySize( keySize );
			( ( Stage ) ( ( Node ) event.getSource() ).getScene().getWindow() ).close();
		} );

		cancelButton.setOnMouseClicked( event ->
		{
			( ( Stage ) ( ( Node ) event.getSource() ).getScene().getWindow() ).close();
		} );
	}

	public final SceneDialogKeyResult getSceneDialogKeyResult()
	{
		return result;
	}
}
