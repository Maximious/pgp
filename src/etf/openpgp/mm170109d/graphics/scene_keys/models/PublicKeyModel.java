package etf.openpgp.mm170109d.graphics.scene_keys.models;

import etf.openpgp.mm170109d.logic.certificates.KeyRings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.bouncycastle.openpgp.PGPPublicKey;

import java.util.Date;

public class PublicKeyModel
{

	private Long id;
	private String hexID;
	private Date dateCreated;
	private Date dateExpire;
	private String user;
	private String alg;
	private int strength;
	private String usage;

	private static ObservableList< PublicKeyModel > list;

	public PublicKeyModel( PGPPublicKey key )
	{
		this.id = key.getKeyID();
		hexID = String.format( "%016X", id );
		hexID = hexID.substring( 0, 4 ) + " " + hexID.substring( 4, 8 ) +
				" " + hexID.substring( 8, 12 ) + " " + hexID.substring( 12 );
		this.dateCreated = key.getCreationTime();

		long validPeriod = key.getCreationTime().getTime();
		long validSeconds = key.getValidSeconds() * 1000;
		validPeriod += validSeconds;
		Date expDate = new Date( validPeriod );
		if( expDate.getTime() > 0 )
			this.dateExpire = expDate;
		this.user = key.getUserIDs().next();
		this.alg = key.getAlgorithm() <= 3 ? "RSA" : "unknown";
		this.strength = key.getBitStrength();
		this.usage = "Encrypt";
	}

	public static ObservableList< PublicKeyModel > getPublicKeys()
	{
		return list;
	}

	public static void generatePublicKeys()
	{
		list = FXCollections.observableArrayList();
		KeyRings.getPublicKeys().forEach(
				key ->
				{
					long validPeriod = key.getCreationTime().getTime();
					long validSeconds = key.getValidSeconds();
					validPeriod += validSeconds * 1000;
					Date expDate = new Date( validPeriod );
					list.add( new PublicKeyModel( key ) );
				}
		);
	}

	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	public Date getDateCreated()
	{
		return dateCreated;
	}

	public void setDateCreated( Date dateCreated )
	{
		this.dateCreated = dateCreated;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser( String user )
	{
		this.user = user;
	}

	public Date getDateExpire()
	{
		return dateExpire;
	}

	public void setDateExpire( Date dateExpire )
	{
		this.dateExpire = dateExpire;
	}

	public String getHexID()
	{
		return hexID;
	}

	public void setHexID( String hexID )
	{
		this.hexID = hexID;
	}

	public String getAlg()
	{
		return alg;
	}

	public void setAlg( String alg )
	{
		this.alg = alg;
	}

	public int getStrength()
	{
		return strength;
	}

	public void setStrength( int strength )
	{
		this.strength = strength;
	}

	public String getUsage()
	{
		return usage;
	}

	public void setUsage( String usage )
	{
		this.usage = usage;
	}

	@Override
	public String toString()
	{
		return user + " (" + hexID + ")";
	}
}
