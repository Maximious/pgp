package etf.openpgp.mm170109d.graphics.scene_keys.models;

import etf.openpgp.mm170109d.logic.certificates.KeyRings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.bouncycastle.openpgp.PGPSecretKey;

import java.util.Date;

public class PrivateKeyModel
{

	private Long id;
	private String hexID;
	private Date dateCreated;
	private Date dateExpire;
	private String user;
	private Long encKeyId;
	private String hexEncKeyId;
	private String alg;
	private int strength;
	private String usage;

	private static ObservableList< PrivateKeyModel > list;

	public PrivateKeyModel( PGPSecretKey key )
	{
		this.id = key.getKeyID();
		hexID = String.format( "%016X", id );
		hexID = hexID.substring( 0, 4 ) + " " + hexID.substring( 4, 8 ) +
				" " + hexID.substring( 8, 12 ) + " " + hexID.substring( 12 );
		this.dateCreated = key.getPublicKey().getCreationTime();

		long validPeriod = key.getPublicKey().getCreationTime().getTime();
		long validSeconds = key.getPublicKey().getValidSeconds() * 1000;
		validPeriod += validSeconds;
		Date expDate = new Date( validPeriod );
		if( expDate.getTime() > 0 )
			this.dateExpire = expDate;
		this.user = key.getUserIDs().next();

		encKeyId = key.getPublicKey().getKeyID();
		hexEncKeyId = String.format( "%016X", encKeyId );
		hexEncKeyId = hexEncKeyId.substring( 0, 4 ) + " " + hexEncKeyId.substring( 4, 8 ) +
				" " + hexEncKeyId.substring( 8, 12 ) + " " + hexEncKeyId.substring( 12 );
		this.alg = key.getPublicKey().getAlgorithm() <= 3 ? "RSA" : "unknown";
		this.strength = key.getPublicKey().getBitStrength();
		;
		this.usage = "";
		if( key.isSigningKey() )
		{
			usage += "Sign ";
		}
	}

	public static ObservableList< PrivateKeyModel > getPrivateKeys()
	{
		return list;
	}

	public static void generatePrivateKeys()
	{
		list = FXCollections.observableArrayList();
		KeyRings.getSecretKeys().forEach(
				key ->
				{
					long validPeriod = key.getPublicKey().getCreationTime().getTime();
					long validSeconds = key.getPublicKey().getValidSeconds();
					validPeriod += validSeconds * 1000;
					Date expDate = new Date( validPeriod );
					list.add( new PrivateKeyModel( key ) );
				}
		);
	}

	public Long getId()
	{
		return id;
	}

	public void setId( Long id )
	{
		this.id = id;
	}

	public String getHexID()
	{
		return hexID;
	}

	public void setHexID( String hexID )
	{
		this.hexID = hexID;
	}

	public Date getDateCreated()
	{
		return dateCreated;
	}

	public void setDateCreated( Date dateCreated )
	{
		this.dateCreated = dateCreated;
	}

	public Date getDateExpire()
	{
		return dateExpire;
	}

	public void setDateExpire( Date dateExpire )
	{
		this.dateExpire = dateExpire;
	}

	public String getUser()
	{
		return user;
	}

	public void setUser( String user )
	{
		this.user = user;
	}

	public Long getEncKeyId()
	{
		return encKeyId;
	}

	public void setEncKeyId( Long encKeyId )
	{
		this.encKeyId = encKeyId;
	}

	public String getHexEncKeyId()
	{
		return hexEncKeyId;
	}

	public void setHexEncKeyId( String hexEncKeyId )
	{
		this.hexEncKeyId = hexEncKeyId;
	}

	public String getAlg()
	{
		return alg;
	}

	public void setAlg( String alg )
	{
		this.alg = alg;
	}

	public int getStrength()
	{
		return strength;
	}

	public void setStrength( int strength )
	{
		this.strength = strength;
	}

	public String getUsage()
	{
		return usage;
	}

	public void setUsage( String usage )
	{
		this.usage = usage;
	}

	@Override
	public String toString()
	{
		return user + " (" + hexID + ")";
	}
}
