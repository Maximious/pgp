package etf.openpgp.mm170109d.graphics.scene_keys;

import etf.openpgp.mm170109d.graphics.scene_keys.models.PrivateKeyModel;
import etf.openpgp.mm170109d.graphics.scene_keys.models.PublicKeyModel;
import etf.openpgp.mm170109d.graphics.scene_keys.scene_dialog_key.SceneDialogKey;
import etf.openpgp.mm170109d.graphics.scene_keys.scene_dialog_key.SceneDialogKeyResult;
import etf.openpgp.mm170109d.logic.certificates.KeyRings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.bouncycastle.openpgp.PGPException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.Optional;
import java.util.ResourceBundle;

public class Controller implements Initializable
{
	@FXML
	private Label tableTitle;

	@FXML
	private TableView< PrivateKeyModel > privateKeysTable;
	@FXML
	private TableColumn< PrivateKeyModel, String > keyIDPrivateColumn;
	@FXML
	private TableColumn< PrivateKeyModel, String > userPrivateColumn;
	@FXML
	private TableColumn< PrivateKeyModel, Date > dateCreatedPrivateColumn;
	@FXML
	private TableColumn< PrivateKeyModel, Date > dateExpirePrivateColumn;
	@FXML
	private TableColumn< PrivateKeyModel, String > encKeyIdPrivateColumn;
	@FXML
	private TableColumn< PrivateKeyModel, String > algPrivateColumn;
	@FXML
	private TableColumn< PrivateKeyModel, Integer > strengthPrivateColumn;
	@FXML
	private TableColumn< PrivateKeyModel, String > usagePrivateColumn;

	@FXML
	private TableView< PublicKeyModel > publicKeysTable;
	@FXML
	private TableColumn< PublicKeyModel, String > keyIDPublicColumn;
	@FXML
	private TableColumn< PublicKeyModel, String > userPublicColumn;
	@FXML
	private TableColumn< PublicKeyModel, Date > dateCreatedPublicColumn;
	@FXML
	private TableColumn< PublicKeyModel, Date > dateExpirePublicColumn;
	@FXML
	private TableColumn< PublicKeyModel, String > algExpirePublicColumn;
	@FXML
	private TableColumn< PublicKeyModel, Integer > strengthPublicColumn;
	@FXML
	private TableColumn< PublicKeyModel, String > usagePublicColumn;

	private int state = 0; //public 0, private 1
	private ObservableList< PrivateKeyModel > privateKeyModelObservableList;
	private ObservableList< PublicKeyModel > publicKeyModelObservableList;

	@Override
	public void initialize( URL url, ResourceBundle resourceBundle )
	{
		PublicKeyModel.generatePublicKeys();
		keyIDPublicColumn.setCellValueFactory( new PropertyValueFactory< PublicKeyModel, String >( "hexID" ) );
		userPublicColumn.setCellValueFactory( new PropertyValueFactory< PublicKeyModel, String >( "user" ) );
		dateCreatedPublicColumn.setCellValueFactory( new PropertyValueFactory< PublicKeyModel, Date >( "dateCreated" ) );
		dateExpirePublicColumn.setCellValueFactory( new PropertyValueFactory< PublicKeyModel, Date >( "dateExpire" ) );
		algExpirePublicColumn.setCellValueFactory( new PropertyValueFactory< PublicKeyModel, String >( "alg" ) );
		strengthPublicColumn.setCellValueFactory( new PropertyValueFactory< PublicKeyModel, Integer >( "strength" ) );
		usagePublicColumn.setCellValueFactory( new PropertyValueFactory< PublicKeyModel, String >( "usage" ) );
		publicKeyModelObservableList = PublicKeyModel.getPublicKeys();
		publicKeysTable.setItems( publicKeyModelObservableList );

		PrivateKeyModel.generatePrivateKeys();
		keyIDPrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, String >( "hexID" ) );
		userPrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, String >( "user" ) );
		dateCreatedPrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, Date >( "dateCreated" ) );
		dateExpirePrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, Date >( "dateExpire" ) );
		encKeyIdPrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, String >( "hexEncKeyId" ) );
		algPrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, String >( "alg" ) );
		strengthPrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, Integer >( "strength" ) );
		usagePrivateColumn.setCellValueFactory( new PropertyValueFactory< PrivateKeyModel, String >( "usage" ) );
		privateKeyModelObservableList = PrivateKeyModel.getPrivateKeys();
		privateKeysTable.setItems( privateKeyModelObservableList );
	}

	public void switchTablePublic( MouseEvent mouseEvent )
	{
		if( state != 0 )
		{
			publicKeysTable.setVisible( true );
			privateKeysTable.setVisible( false );
			tableTitle.setText( "Public keys" );
			state = 0;
		}
	}

	public void switchTablePrivate( MouseEvent mouseEvent )
	{
		if( state != 1 )
		{
			publicKeysTable.setVisible( false );
			privateKeysTable.setVisible( true );
			tableTitle.setText( "Private keys" );
			state = 1;
		}
	}

	public void addKey( MouseEvent mouseEvent )
	{
		Image keyImage = new Image( "etf/openpgp/mm170109d/graphics/scene_keys/images/key.png" );

		SceneDialogKey dialogKey;
		try
		{
			dialogKey = new SceneDialogKey();
			Stage stage = new Stage();
			stage.setTitle( "Key details" );
			stage.getIcons().add( keyImage );
			stage.setScene( new Scene( dialogKey ) );
			stage.showAndWait();
		} catch( IOException e )
		{
			e.printStackTrace();
			return;
		}

		SceneDialogKeyResult result = dialogKey.getSceneDialogKeyResult();
		if( result == null )
			return;

		char[] pass;

		Dialog< String > dialog = new Dialog<>();
		dialog.setTitle( "Secret key password" );
		( ( Stage ) dialog.getDialogPane().getScene().getWindow() ).getIcons().add( keyImage );
		dialog.getDialogPane().getButtonTypes().addAll( ButtonType.OK, ButtonType.CANCEL );

		PasswordField passwordField = new PasswordField();
		HBox content = new HBox();
		content.setAlignment( Pos.CENTER_LEFT );
		content.setSpacing( 10 );
		content.getChildren().addAll( new Label( "Password:" ), passwordField );
		dialog.getDialogPane().setContent( content );
		dialog.setResultConverter( dialogButton ->
		{
			if( dialogButton == ButtonType.OK )
			{
				return passwordField.getText();
			}
			return null;
		} );

		Optional< String > passResult = dialog.showAndWait();
		if( passResult.isPresent() )
		{
			pass = passResult.get().toCharArray();
			//System.out.println("Password = " + " " + passResult.get());
		} else
		{
			return;
		}

		privateKeyModelObservableList.add( new PrivateKeyModel(
				KeyRings.createNewSecretKey( result.getId(), pass, result.getKeySize() ) )
		);

		switchTablePrivate( null );
	}

	public void deleteKey( MouseEvent mouseEvent )
	{
		if( ( state == 0 && publicKeysTable.getSelectionModel().getSelectedItem() == null ) ||
				( state == 1 && privateKeysTable.getSelectionModel().getSelectedItem() == null ) )
			return;

		Image deleteImage = new Image( "etf/openpgp/mm170109d/graphics/scene_keys/images/minus.png" );

		Alert confirmDelete = new Alert( Alert.AlertType.CONFIRMATION );
		Stage stage = ( Stage ) confirmDelete.getDialogPane().getScene().getWindow();
		stage.getIcons().add( deleteImage );
		confirmDelete.setHeaderText( null );
		confirmDelete.setGraphic( null );
		confirmDelete.setTitle( "Are you sure?" );
		confirmDelete.setContentText( "Do you really want to delete the selected key?" );

		Optional< ButtonType > result = confirmDelete.showAndWait();
		if( result.get() == ButtonType.OK )
		{
			if( state == 0 )
			{
				PublicKeyModel keyModel = publicKeysTable.getSelectionModel().getSelectedItem();
				if( keyModel == null )
				{
					return;
				}
				KeyRings.removePublicKey( keyModel.getId() );
				publicKeyModelObservableList.remove( publicKeysTable.getSelectionModel().getSelectedIndex() );
			} else
			{
				PrivateKeyModel keyModel = privateKeysTable.getSelectionModel().getSelectedItem();
				if( keyModel == null )
				{
					return;
				}

				char[] pass;
				Dialog< String > dialog = new Dialog<>();
				dialog.setTitle( "Secret key password" );
				( ( Stage ) dialog.getDialogPane().getScene().getWindow() ).getIcons()
						.add( new Image( "etf/openpgp/mm170109d/graphics/scene_keys/images/key.png" ) );
				dialog.getDialogPane().getButtonTypes().addAll( ButtonType.OK, ButtonType.CANCEL );

				PasswordField passwordField = new PasswordField();
				HBox content = new HBox();
				content.setAlignment( Pos.CENTER_LEFT );
				content.setSpacing( 10 );
				content.getChildren().addAll( new Label( "Password:" ), passwordField );
				dialog.getDialogPane().setContent( content );
				dialog.setResultConverter( dialogButton ->
				{
					if( dialogButton == ButtonType.OK )
					{
						return passwordField.getText();
					}
					return null;
				} );

				Optional< String > passResult = dialog.showAndWait();
				if( passResult.isPresent() )
				{
					pass = passResult.get().toCharArray();
				} else
				{
					return;
				}

				if( KeyRings.checkPasswordOk( keyModel.getId(), pass ) )
				{
					KeyRings.removeSecretKey( keyModel.getId() );
					privateKeyModelObservableList.remove( privateKeysTable.getSelectionModel().getSelectedIndex() );
				} else
				{
					//Alert notificationAlert = new Alert( Alert.AlertType.INFORMATION );
					confirmDelete.setHeaderText( null );
					confirmDelete.setGraphic( null );
					confirmDelete.setTitle( "Wrong password!" );
					confirmDelete.setContentText( "Password you have entered is invalid!!!" );
					confirmDelete.show();
				}
			}
		}
	}

	public void exportKey( MouseEvent mouseEvent )
	{
		DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle( "Select location for new key" );
		Stage stage = new Stage();
		File file = directoryChooser.showDialog( stage );

		if( file != null )
		{
			String path = file.getPath();
			if( state == 0 )
			{
				PublicKeyModel keyModel = publicKeysTable.getSelectionModel().getSelectedItem();
				if( keyModel == null )
				{
					return;
				}
				try
				{
					//KeyRings.exportKey( KeyRings.getPublicKey( keyModel.getId() ), null);
					KeyRings.exportKey( KeyRings.getPublicKeyRing( keyModel.getId() ), null, path );
				} catch( PGPException e )
				{
					e.printStackTrace();
				}
			} else
			{
				PrivateKeyModel keyModel = privateKeysTable.getSelectionModel().getSelectedItem();
				if( keyModel == null )
				{
					return;
				}
				try
				{
					//KeyRings.exportKey( null, KeyRings.getSecretKey( keyModel.getId() ));
					KeyRings.exportKey( null, KeyRings.getSecretKeyRing( keyModel.getId() ), path );
				} catch( PGPException e )
				{
					e.printStackTrace();
				}
			}
		}
	}

	public void importKey( MouseEvent mouseEvent )
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle( "Select new key" );
		Stage stage = new Stage();
		File file = fileChooser.showOpenDialog( stage );

		if( file != null )
		{
			String path = file.getPath();
			publicKeyModelObservableList.add( new PublicKeyModel( KeyRings.importKey( path ) ) );
		}
	}

	public void back( MouseEvent mouseEvent )
	{
		Parent root = null;
		try
		{
			root = FXMLLoader.load( getClass().getResource( "../scene_main/scene.fxml" ) );
		} catch( IOException e )
		{
			e.printStackTrace();
		}
		Stage stage = ( Stage ) ( ( Node ) mouseEvent.getSource() ).getScene().getWindow();
		stage.getScene().setRoot( root );
		stage.show();
	}
}
