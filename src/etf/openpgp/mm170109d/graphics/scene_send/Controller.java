package etf.openpgp.mm170109d.graphics.scene_send;

import etf.openpgp.mm170109d.graphics.scene_keys.models.PrivateKeyModel;
import etf.openpgp.mm170109d.graphics.scene_keys.models.PublicKeyModel;
import etf.openpgp.mm170109d.logic.certificates.KeyRings;
import etf.openpgp.mm170109d.logic.pgp.PGP;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.bouncycastle.openpgp.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Optional;
import java.util.ResourceBundle;

public class Controller implements Initializable
{

	@FXML
	TextField inputFileLocation;
	@FXML
	TextField outputFileLocation;
	@FXML
	CheckBox encryptCheckBox;
	@FXML
	CheckBox signCheckBox;
	@FXML
	CheckBox compressCheckBox;
	@FXML
	CheckBox radixCheckBox;
	@FXML
	RadioButton tDesRadio;
	@FXML
	RadioButton aesRadio;
	@FXML
	ListView< PublicKeyModel > encKeysList;
	@FXML
	ListView< PrivateKeyModel > sigKeysList;

	private String fileName;

	@Override
	public void initialize( URL url, ResourceBundle resourceBundle )
	{
		encKeysList.getSelectionModel().setSelectionMode( SelectionMode.MULTIPLE );
		sigKeysList.getSelectionModel().setSelectionMode( SelectionMode.SINGLE );
		KeyRings.getPublicKeys().forEach( key -> encKeysList.getItems().add( new PublicKeyModel( key ) ) );
		KeyRings.getSecretKeys().forEach( key -> sigKeysList.getItems().add( new PrivateKeyModel( key ) ) );
	}

	public void mainScene( MouseEvent mouseEvent ) throws IOException
	{
		Parent root = FXMLLoader.load( getClass().getResource( "../scene_main/scene.fxml" ) );
		Stage stage = ( Stage ) ( ( Node ) mouseEvent.getSource() ).getScene().getWindow();
		stage.getScene().setRoot( root );
		stage.show();
	}

	public void setInputFile( MouseEvent mouseEvent )
	{
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle( "Select input file" );
		Stage stage = new Stage();
		File file = fileChooser.showOpenDialog( stage );

		if( file != null )
		{
			String path = file.getPath();
			inputFileLocation.setText( path );
			fileName = path.substring( path.lastIndexOf( '\\' ) + 1 );
		}
	}

	public void setOutputFile( MouseEvent mouseEvent )
	{
		DirectoryChooser directoryChooser = new DirectoryChooser();
		directoryChooser.setTitle( "Select output directory" );
		Stage stage = new Stage();
		File file = directoryChooser.showDialog( stage );

		if( file != null )
		{
			String path = file.getPath();
			outputFileLocation.setText( path + "\\" + fileName + ".pgp" );
		}
	}

	public void startPGP( MouseEvent mouseEvent ) throws PGPException
	{
		ObservableList< PublicKeyModel > selectedPublicKeys = encKeysList.getSelectionModel().getSelectedItems();
		ArrayList< PGPPublicKey > listPublicKeys = new ArrayList< PGPPublicKey >();

		for( PublicKeyModel pkModel : selectedPublicKeys )
		{
			PGPPublicKeyRing keyRing = KeyRings.getPublicKeyRing( pkModel.getId() );
			Iterator< PGPPublicKey > it = keyRing.getPublicKeys();
			PGPPublicKey encKey = it.next();

			while( !encKey.isEncryptionKey() && it.hasNext() )
			{
				encKey = it.next();
			}

			listPublicKeys.add( encKey );
		}

		ObservableList< PrivateKeyModel > selectedPrivateKeys = sigKeysList.getSelectionModel().getSelectedItems();
		ArrayList< PGPSecretKey > listSecretKeys = new ArrayList< PGPSecretKey >();
		PGPSecretKey secretKey = null;

		for( PrivateKeyModel pkModel : selectedPrivateKeys )
		{
			PGPSecretKeyRing keyRing = KeyRings.getSecretKeyRing( pkModel.getId() );
			Iterator< PGPSecretKey > it = keyRing.getSecretKeys();
			PGPSecretKey signKey = it.next();

			while( !signKey.isSigningKey() && it.hasNext() )
			{
				signKey = it.next();
			}

			listSecretKeys.add( signKey );
		}

		if( listSecretKeys.size() > 0 )
		{
			secretKey = KeyRings.getSecretKey( listSecretKeys.get( 0 ).getKeyID() );
		}

		char[] password = null;
		if( signCheckBox.isSelected() )
		{
			Image keyImage = new Image( "etf/openpgp/mm170109d/graphics/scene_keys/images/key.png" );
			Dialog< String > dialog = new Dialog<>();
			dialog.setTitle( "Secret key password" );
			( ( Stage ) dialog.getDialogPane().getScene().getWindow() ).getIcons().add( keyImage );
			dialog.getDialogPane().getButtonTypes().addAll( ButtonType.OK, ButtonType.CANCEL );

			PasswordField passwordField = new PasswordField();
			HBox content = new HBox();
			content.setAlignment( Pos.CENTER_LEFT );
			content.setSpacing( 10 );
			content.getChildren().addAll( new Label( "Password:" ), passwordField );
			dialog.getDialogPane().setContent( content );
			dialog.setResultConverter( dialogButton ->
			{
				if( dialogButton == ButtonType.OK )
				{
					return passwordField.getText();
				}
				return null;
			} );

			Optional< String > passResult = dialog.showAndWait();
			if( passResult.isPresent() )
			{
				password = passResult.get().toCharArray();
				//System.out.println("Password = " + " " + passResult.get());
			} else
			{
				return;
			}
		}

		int algKey = tDesRadio.isSelected() ? PGPEncryptedData.TRIPLE_DES : PGPEncryptedData.AES_128;

		Alert errorInfoAlert = new Alert( Alert.AlertType.ERROR );
		Stage stage = ( Stage ) errorInfoAlert.getDialogPane().getScene().getWindow();
		stage.getIcons().add( new Image( "etf/openpgp/mm170109d/graphics/scene_send/images/error.png" ) );
		errorInfoAlert.setHeaderText( null );
		errorInfoAlert.setGraphic( null );
		errorInfoAlert.setTitle( "Error!" );
		boolean error = false;

		if( inputFileLocation.getText() == null || inputFileLocation.getText().length() == 0 )
		{
			errorInfoAlert.setContentText( "You have to enter input file location!" );
			error = true;
		} else if( outputFileLocation.getText() == null || outputFileLocation.getText().length() == 0 )
		{
			errorInfoAlert.setContentText( "You have to enter output location!" );
			error = true;
		} else if( encryptCheckBox.isSelected() == false && signCheckBox.isSelected() == false )
		{
			errorInfoAlert.setContentText( "You have to either sign or encrypt a file!" );
			error = true;
		} else if( encryptCheckBox.isSelected() && listPublicKeys.size() == 0 )
		{
			errorInfoAlert.setContentText( "You have to select at least one receiver!" );
			error = true;
		} else if( signCheckBox.isSelected() && listSecretKeys.size() == 0 )
		{
			errorInfoAlert.setContentText( "You have to select a secret key to sign the file!" );
			error = true;
		}

		if( error )
		{
			errorInfoAlert.show();
			return;
		}

		String fileName = inputFileLocation.getText().substring( inputFileLocation.getText().lastIndexOf( '\\' ) + 1 );

		try
		{
			PGP.executePGP(
					inputFileLocation.getText(), outputFileLocation.getText(),
					algKey, listPublicKeys, secretKey, password,
					encryptCheckBox.isSelected(), signCheckBox.isSelected(),
					compressCheckBox.isSelected(), radixCheckBox.isSelected()
			);

			Alert doneAlert = new Alert( Alert.AlertType.INFORMATION );
			( ( Stage ) doneAlert.getDialogPane().getScene().getWindow() )
					.getIcons().add( new Image( "etf/openpgp/mm170109d/graphics/scene_send/images/success.png" ) );
			doneAlert.setHeaderText( null );
			doneAlert.setGraphic( null );
			doneAlert.setTitle( "Success!" );
			doneAlert.setContentText( "Operation done!" );
			doneAlert.show();

		} catch( Exception e )
		{
			errorInfoAlert.setContentText( "Something went wrong!" );
			errorInfoAlert.show();
			e.printStackTrace();
		}
	}
}
