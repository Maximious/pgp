package etf.openpgp.mm170109d.graphics.scene_main;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller
{
	public void sceneSend( MouseEvent mouseEvent ) throws IOException
	{
		Parent root = FXMLLoader.load( getClass().getResource( "../scene_send/scene.fxml" ) );
		Stage stage = ( Stage ) ( ( Node ) mouseEvent.getSource() ).getScene().getWindow();
		stage.getScene().setRoot( root );
		stage.show();
	}

	public void sceneRec( MouseEvent mouseEvent ) throws IOException
	{
		Parent root = FXMLLoader.load( getClass().getResource( "../scene_rec/scene.fxml" ) );
		Stage stage = ( Stage ) ( ( Node ) mouseEvent.getSource() ).getScene().getWindow();
		stage.getScene().setRoot( root );
		stage.show();
	}

	public void sceneKeys( MouseEvent mouseEvent ) throws IOException
	{
		Parent root = FXMLLoader.load( getClass().getResource( "../scene_keys/scene.fxml" ) );
		Stage stage = ( Stage ) ( ( Node ) mouseEvent.getSource() ).getScene().getWindow();
		stage.getScene().setRoot( root );
		stage.show();
	}
}
